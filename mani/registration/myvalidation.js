
function validateForm() {
    var name = document.registration.name;

    var phoneNumber = document.registration.phoneNumber;

    var email = document.registration.email;
    
    
    var password1 = document.getElementById("password").value;
    var password2 = document.getElementById("passwordre").value;
    
    const birthday = document.getElementById('bornday');
    const birthdayValue = birthday.value.trim();
    var today = new Date();
    var date = today.getFullYear()
    var d = birthdayValue.substring(0,4);
    let text = "Above 50, click here for help";
    let result = text.link("https://www.w3schools.com");
    age = date-d
        if (age<'13'){
            document.getElementById("dateinvalid").innerHTML= "Age 13+ need parental supervision";
            bornday.focus();
            return false;

        } else if (age>'50'){
            document.getElementById("dateinvalid").innerHTML= result;

            return false;
        }

    if(name.value.length <= 0) {
        alert("Please provide Name");
        name.focus();
        return false;
    }
    if(email.value.length <= 0) {
        alert("Please provide Email");
        email.focus();
        return false;
    }
    if(phoneNumber.value.length <= 0) {
        alert("Please provide Phone number with country code +61");
        phoneNumber.focus();
        return false;
    } 

    if(password1 == ""){
        document.getElementById("message").innerHTML="**Please enter Password";
        return false;
    }
    if(password1.length < 5 ){
        document.getElementById("message").innerHTML="**Password Length is minimum 5";
        return false;
    }
    if(password1.length > 25 ){
        document.getElementById("message").innerHTML="** Password Length is Maximum 25";
        password1.focus();
        return false;
    }
    
    if(password1 != password2){
        document.getElementById("message").innerHTML= "**Please enter same Passwords";
        return false;
    }
    
        // return false;
    }
    
    function CheckPasswordStrength(password) {
        var password_strength = document.getElementById("passcode_strength");
        
                //TextBox left blank.
                if (password.length == 0) {
                    password_strength.innerHTML = "";
                    return;
                }
        
                //Regular Expressions.
                var regex = new Array();
                regex.push("[A-Z]"); //Uppercase Alphabet.
                regex.push("[a-z]"); //Lowercase Alphabet.
                regex.push("[0-9]"); //Digit.
        
                var passed = 0;
        
                //Validate for each Regular Expression.
                for (var i = 0; i < regex.length; i++) {
                    if (new RegExp(regex[i]).test(password)) {
                        passed++;
                    }
                }
        
                //Display status.
                var color = "";
                var strength = "";
                switch (passed) {
                    case 0:
                    case 1:
                    case 2:
                        strength = "Weak";
                        color = "red";
                        break;
                    case 3:
                         strength = "Medium";
                        color = "orange";
                        break;
                    case 4:
                         strength = "Strong";
                        color = "green";
                        break;
                       
                }
                password_strength.innerHTML = strength;
                password_strength.style.color = color;
        
        if(passed <= 2){
                 document.getElementById('show').disabled = true;
                }else{
                    document.getElementById('show').disabled = false;
                }
        
            }

    
let autocomplete;
let address1Field;
let address2Field;
let postalField;

function initAutocomplete() {
  address1Field = document.querySelector("#home-address");
  address2Field = document.querySelector("#street");
  postalField = document.querySelector("#postCode");
  // Create the autocomplete object, restricting the search predictions to
  // addresses in the US and Canada.
  autocomplete = new google.maps.places.Autocomplete(address1Field, {
    componentRestrictions: { country: ["au"] },
    fields: ["address_components", "geometry"],
    types: ["address"],
  });
  address1Field.focus();
  // When the user selects an address from the drop-down, populate the
  // address fields in the form.
  autocomplete.addListener("place_changed", fillInAddress);
}

function fillInAddress() {
  // Get the place details from the autocomplete object.
  const place = autocomplete.getPlace();
  let address1 = "";
  let postcode = "";

  // Get each component of the address from the place details,
  // and then fill-in the corresponding field on the form.
  // place.address_components are google.maps.GeocoderAddressComponent objects
  // which are documented at http://goo.gle/3l5i5Mr
  for (const component of place.address_components) {
    const componentType = component.types[0];

    switch (componentType) {
      case "street_number": {
        address1 = `${component.long_name} ${address1}`;
        break;
      }

      case "route": {
        address1 += component.short_name;
        break;
      }

      case "postal_code": {
        postcode = `${component.long_name}${postcode}`;
        break;
      }

      case "postal_code_suffix": {
        postcode = `${postcode}-${component.long_name}`;
        break;
      }
      case "locality":
        document.querySelector("#suburb").value = component.long_name;
        break;
      case "administrative_area_level_1": {
        document.querySelector("#state").value = component.short_name;
        break;
      }
      case "country":
        document.querySelector("#country").value = component.long_name;
        break;
    }
  }

  address1Field.value = address1;
  postalField.value = postcode;
  // After filling the form with address components from the Autocomplete
  // prediction, set cursor focus on the second address line to encourage
  // entry of subpremise information such as apartment, unit, or floor number.
   address2Field.focus();
}