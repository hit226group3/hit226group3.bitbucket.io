// 
$(function () {
    $.getJSON('./script/shop.json', function (data) {
        let products = JSON.parse(localStorage.getItem("myCart"));
        if (products) {

            products.products.map((item, index) => {
                var id = item.id
                var prod = ` <tr id=table_cart_row${id}>
              <td>
                  <div>
                    <img src=${data[item.id - 1].image} alt="">
                    <div class="info">
                        <p>${data[item.id - 1].name} ${id}</p>
                        <h5>${data[item.id - 1].currency}${data[item.id-1].price}</h5>
                    </div>
                  </div>
              </td>
              <td><div class="quantity">
                <i class="fa fa-minus"  onClick=decrementCart(${id})></i>
                <p id="table_cart_quantity${id}">${item.quantity}</p>
                <i class="fa fa-plus" onClick=incrementCart(${id})></i>
            </div></td>
              <td onClick=removeFromCart(${id})><i class="far fa-times"></i></td>
            </tr>`
                $(prod).appendTo("#cart_table");
            })
            totalPrice()

        }
    });
});

// calculate the price after adding
function incrementCart(id) {
    let products = JSON.parse(localStorage.getItem("myCart"));
    let index = indexofProduct(id,products)
    products.products[index].quantity += 1;
    document.getElementById(`table_cart_quantity${id}`).innerHTML = products.products[index].quantity
    localStorage.setItem("myCart", JSON.stringify(products))
    getCart()
    getCartMobile()
    totalPrice()
}

//  calculate the price after removal
function decrementCart(id) {
    let products = JSON.parse(localStorage.getItem("myCart"));
    let index = indexofProduct(id,products)
    if (products.products[index].quantity > 1) {
        products.products[index].quantity -= 1;
        document.getElementById(`table_cart_quantity${id}`).innerHTML = products.products[index].quantity
        localStorage.setItem("myCart", JSON.stringify(products))
        getCart()
        getCartMobile()
        totalPrice()
    }
}

// increment of product

function indexofProduct(id,products){
    
         return  products.products.findIndex(item=>{return item.id == id})
    
}

function parser(id,products){
    var product = null
    let x = products.productId.filter(product=>product !=id);
    let y =  products.products.filter(product=>product.id !=id);
    let body = {
        productId:x,
        products:y
    }
      product = body
      return product
}

// decrement of product
async function removeFromCart(id) {
    var products = JSON.parse(localStorage.getItem("myCart"))
    
    let newData =await parser(id,products)
    localStorage.setItem("myCart", JSON.stringify(newData))
     getCart()
     getCartMobile()
     totalPrice()
     document.getElementById(`table_cart_row${id}`).outerHTML = "";
}

function totalPrice() {
    var TOTAL = 0;
    $.getJSON('./script/shop.json', function (data) {
        let products = JSON.parse(localStorage.getItem("myCart"));
        if (products) {
            products.products.map((item, index) => {
                TOTAL += data[item.id].price * item.quantity

            })
        }
        document.getElementById("cart_total_price").innerHTML = `&nbsp;$ ${TOTAL}`
    });
}

function getCart() {
    let products = JSON.parse(localStorage.getItem("myCart"));

    if (products?.productId?.length > 0) {
        document.getElementById("cart_header").style.display = "flex"
        document.getElementById("cart_header").innerHTML = products?.productId?.length
    }
    else {
        document.getElementById("cart_header").style.display = "none"
    }
}

function getCartMobile() {
    let products = JSON.parse(localStorage.getItem("myCart"));

    if (products?.productId?.length > 0) {
        // return products?.productId?.length
        document.getElementById("cart_header_mobile").style.display = "flex";
        document.getElementById("cart_header_mobile").innerHTML =
            products?.productId?.length;
    } else {
        document.getElementById("cart_header_mobile").style.display = "none";
    }
}


 // Render the PayPal button into #paypal-button-container
paypal
 .Buttons({
     // Set up the transaction
     createOrder: function (data, actions) {
         return actions.order.create({
             purchase_units: [
                 {
                     amount: {
                         value: "88.44",
                     },
                 },
             ],
         });
     },

     // Finalize the transaction
     onApprove: function (data, actions) {
         return actions.order.capture().then(function (orderData) {
             // Successful capture! For demo purposes:
             console.log(
                 "Capture result",
                 orderData,
                 JSON.stringify(orderData, null, 2)
             );
             var transaction =
                 orderData.purchase_units[0].payments.captures[0];
             alert(
                 "Transaction " +
                 transaction.status +
                 ": " +
                 transaction.id +
                 "\n\nSee console for all available details"
             );
             const cart = {
                     productId: [],
                     products: [],
                 };
                 localStorage.setItem("myCart", JSON.stringify(cart));
                 window.location = 'index.html'
             // localStorage.clear()
             // actions.redirect('index.html');

             // Replace the above to show a success message within this page, e.g.
             // const element = document.getElementById('paypal-button-container');
             // element.innerHTML = '';
             // element.innerHTML = '<h3>Thank you for your payment!</h3>';
             // Or go to another URL:  actions.redirect('thank_you.html');
         });
     },
 })
 .render("#paypal-button-container");