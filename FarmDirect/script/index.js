// generate featured product cards in home page
$(function() {
    $.getJSON('./script/shop.json', function(data) {
     data.map((item,index)=>{
         if(item?.featured){
         var stars = new Array(item?.star).fill(item?.star)
         var icons = []
         stars.map((item)=>{icons.push(`<i class="fa fa-star"></i>`)})
         var id = item.productId
         var prod = `
         <div class="pro-container">
             <div class="pro">
                 <img src="${item.image}" class="cardimage" />
                 <div class="des">
                     <span class="vendor">${item.store} </span>
                     <h5 class="productName">${item.name}</h5>
                     <div class="star">
                          ${icons.join().replace(/,/g,"")}
                     </div>
                     <h4 class="productName">${item.currency} ${item.price}</h4>
                     </div>
                     <a onClick=addCart(${id})><i class="fal fa-shopping-cart cart"></i></a>
                 </div>
             </div>
         `
            $(prod).appendTo("#product1");
         }
     })
    });
 });