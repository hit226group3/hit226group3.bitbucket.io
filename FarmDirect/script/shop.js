
// generate cards in shop page
$(function() {
    $.getJSON('./script/shop.json', function(data) {
     const urlParams = new URLSearchParams(window.location.search);
     const page = urlParams.get('page');
     var pageData = data
     if(page == 2)
     {
         pageData = data.slice(data.length/2,data.length)
     }
     else{
         pageData = data.slice(0,data.length/2)
     }
     pageData.map((item,index)=>{

        var id = item.productId
         var prod = `
         <div class="pro-container">
             <div class="pro">
                 <img src="${item.image}" class="cardimage" />
                 <div class="des">
                     <span class="vendor">${item.store} </span>
                     <h5 class="productName">${item.name}</h5>
                     <h4 class="productName">${item.currency} ${item.price}</h4>
                     </div>
                     <a onClick=addCart(${id})><i class="fal fa-shopping-cart cart"></i></a>
                 </div>
             </div>
         `
            $(prod).appendTo("#product1");
     })
    });
 });