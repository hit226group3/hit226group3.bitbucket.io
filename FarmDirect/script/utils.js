function getCart() {
  let products = JSON.parse(localStorage.getItem("myCart"));

  if (products?.productId?.length > 0) {
    // return products?.productId?.length
    document.getElementById("cart_header").style.display = "flex";
    document.getElementById("cart_header").innerHTML =
      products?.productId?.length;
  } else {
    document.getElementById("cart_header").style.display = "none";
  }
}
function getCartMobile() {
  let products = JSON.parse(localStorage.getItem("myCart"));

  if (products?.productId?.length > 0) {
    // return products?.productId?.length
    document.getElementById("cart_header_mobile").style.display = "flex";
    document.getElementById("cart_header_mobile").innerHTML =
      products?.productId?.length;
  } else {
    document.getElementById("cart_header_mobile").style.display = "none";
  }
}


function addCart(item) {
  const cart = {
    productId: [],
    products: [],
  };
  if (!localStorage.getItem("myCart"))
    localStorage.setItem("myCart", JSON.stringify(cart));

  let products = JSON.parse(localStorage.getItem("myCart"));
  if (products.productId.includes(item)) {
    alert("Product already added to the cart");
  } else {
    let body = { id: item, quantity: 1 };
    products.products.push(body);
    products.productId.push(item);

    localStorage.setItem("myCart", JSON.stringify(products));
    getCart();
    getCartMobile();
  }
}
