const bar = document.getElementById('bar');
const close = document.getElementById('close');
const nav = document.getElementById('navbar');

if (bar) {
    bar.addEventListener('click', () => {
        nav.classList.add('active');
    })
}

if (close) {
    close.addEventListener('click', () => {
        nav.classList.remove('active');
    })
}


//  search products in all page function
function searchProducts() {
  let search_text = document.getElementById("search_text").value;
  if (search_text.length > 0) {
    window.location = `search.html?s=${search_text}`;
  }
}