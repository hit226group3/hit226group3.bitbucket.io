$(function(){
    const urlParams = new URLSearchParams(window.location.search);
    const productName = urlParams.get('s');
    document.getElementById("search_text").value = productName

    $.getJSON('./script/shop.json', function(data) {

    let result = data.filter(item => item.name.toLowerCase().search(productName.toLowerCase()) !== -1);
    if(result.length>0){
        result.map((item,index)=>{
            var id = item.productId
            var prod = `
    <div class="pro-container" onClick=addCart(${index})>
        <div class="pro">
            <img src="${item.image}" class="cardimage" />
            <div class="des">
                <span class="vendor">${item.store} </span>
                <h5 class="productName">${item.name}</h5>
                <h4 class="productName">${item.currency} ${item.price}</h4>
                </div>
                <a onClick=addCart(${id})><i class="fal fa-shopping-cart cart"></i></a>
            </div>
        </div>
    ` 
    $(prod).appendTo("#product_search_result");
        })
    }
    })


})